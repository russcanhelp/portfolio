import React from 'react'

const Modal = ({ isVisible, onClose, children }) => {
    if ( !isVisible ) return null;
    const handleClose = (e) => {
        if (e.target.id === 'wrapper' ) onClose();
    }
  return (
    <div onClick={handleClose} id='wrapper' className='fixed inset-0 bg-black bg-opacity-25 backdrop-blur-sm flex justify-center items-center'>
        <div className='md:w-[600px] w-[90%] mx-auto flex flex-col'>
            <button onClick={() => onClose()} className='text-white text-xl place-self-end'>x</button>
            <div className='bg-[#333333] text-gray-300 rounded-lg text-xl p-4 overflow-auto max-h-[60vh]'>{children}</div>
        </div>
    </div>

    )
}

export default Modal