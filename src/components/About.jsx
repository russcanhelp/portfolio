import React from 'react'
import Pic from '../assets/Russ.jpeg'

const About = () => {
  return (
    <div name='about' className='w-full h-screen bg-[#333333] text-gray-300 p-8'>
        <div className='flex flex-col justify-center items-center w-full h-full'>
            <div className='max-w-[1000px] w-full grid grid-cols-2 gap-8'>
                <div className='sm:text-start pb-8 pl-4'>
                    <p className='text-4xl font-bold inline border-b-4 border-yellow-600'>
                    About
                    </p>
                </div>
                <div></div>
            </div>
            <div className='max-w-[1000px] w-full grid sm:grid-cols-2 gap-8 px-4'>
                <div className='sm:flex justify-center items-center'>
                    <img className='rounded-full' src={Pic} alt='profile_pic' style={{width:'250px'}} />
                </div>
                <div className='sm:text-left text-l flex items-center'>
                    <p>
                    "Hi! I'm Russ, a Software Engineer based around Los Angeles.📍 My background
                    is in Finance where I mastered crafting personalized financial
                    plans for clients based on needs.📚 I was very intrigued with the power of tech in revolutionizing my client
                    interactions.📈 So much, That i left my career to pursue software engineering, diving directly into full stack development.💻
                    Now, I'm excited to apply my tech skills with the ability to deeply understand client needs.💥"
                    </p>
                </div>
            </div>
            </div>
        </div>
  )
}

export default About