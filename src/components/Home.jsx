import React from 'react'
import {HiArrowNarrowRight} from 'react-icons/hi'
import {Link} from 'react-scroll'
import GTR from '../assets/GTR.mp4'

const Home = () => {
  return (
    <div name='home' className='w-full h-screen bg-[#272727]'>
      <video src={GTR} autoPlay loop muted playsInline preload style={{width:'100%', height:'100%', objectFit:'cover', filter: 'brightness(40%)'}}/>
        {/* container */}
        <div className='home-content'>
        <div className='max-w-[1000px] mx-auto px-8 flex flex-col justify-center h-full'>
            <p className='text-yellow-600'>Hi, my name is</p>
            <h1 className='text-4xl sm:text-7xl font-bold text-[#ccd6f6]'>Russell Cruz</h1>
            <h2 className='text-4xl sm:text-7xl font-bold text-gray-400'>I'm a Full Stack Developer.</h2>
            <p className='text-gray-400 py-4 max-w font-bold'>I'm also a car enthusiast, entrepreneur, gamer, and amateur photographer/videographer. Video background credits to Woyshinis Media.</p>

            <div>
                <button className='text-white group border-2 px-6 py-3 my-2 flex items-center hover:bg-yellow-600 hover:border-yellow-600'>
                    <Link to="work" smooth={true} duration={500}>
                    View Work
                    </Link>
                    <span className='group-hover:rotate-90 duration-300'>
                    <HiArrowNarrowRight className='ml-3'/>
                    </span>
                    </button>

            </div>
        </div>
      </div>
    </div>
  )
}

export default Home