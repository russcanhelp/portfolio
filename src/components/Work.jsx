import React, { useState } from 'react'
import AOP from '../assets/AttackOnPython.png'
import Modal from './Modal';

const Work = () => {
    const [showAOP, setShowModal] = useState(false);

  return (
    <div name='work' className= 'w-full md:h-screen bg-[#333333] text-gray-300 p-8'>
        <div className='max-w-[1000px] mx-auto p-4 flex flex-col justify-center w-full h-full'>
            <div className='pb-4'>
                <p className='text-4xl font-bold inline border-b-4 text-gray-300 border-yellow-600'>Work</p>
                <p className='py-4'>Check out some of my recent work</p>
            </div>


            <div className='grid sm:grid-cols-2 md:grid-cols-3 gap-4'>
                {/* grid item */}
                <div style={{backgroundImage: `url(${AOP})`}} className='shadow-lg shadow-[#040c16] group container rounded-md flex justify-center items-center mx-auto content-div'>

                   {/* hover effects */}
                    <div className='opacity-0 group-hover:opacity-100'>
                        <span className='text-2xl font-bold text-white tracking-wider flex justify-center items-center'>
                            Attack on Python
                        </span>
                        <div className='pt-8 text-center'>
                            <button onClick={() => setShowModal(true)} className='text-center rounded-lg px-4 py-3 m-2 text-gray-700 font-bold text-lg bg-white'>Demo</button>
                            <a href='https://gitlab.com/attack-on-python/attack-on-python' target='_blank' rel='noreferrer'>
                                <button className='text-center rounded-lg px-4 py-3 m-2 text-gray-700 font-bold text-lg bg-white'>Code</button>
                            </a>
                        </div>
                    </div>
                </div>

                {/* grid item */}
                <div style={{backgroundImage: `url(${AOP})`}} className='shadow-lg shadow-[#040c16] group container rounded-md flex justify-center items-center mx-auto content-div'>

                   {/* hover effects */}
                    <div className='opacity-0 group-hover:opacity-100'>
                        <span className='text-2xl font-bold text-white tracking-wider flex justify-center items-center'>
                            CarSense
                        </span>
                        <div className='pt-8 text-center'>
                                <button onClick={() => setShowModal(true)} className='text-center rounded-lg px-4 py-3 m-2 text-gray-700 font-bold text-lg bg-white'>Demo</button>
                            <a href='https://gitlab.com/attack-on-python/attack-on-python' target='_blank' rel='noreferrer'>
                                <button className='text-center rounded-lg px-4 py-3 m-2 text-gray-700 font-bold text-lg bg-white'>Code</button>
                            </a>
                        </div>
                    </div>
                </div>

                {/* grid item */}
                <div style={{backgroundImage: `url(${AOP})`}} className='shadow-lg shadow-[#040c16] group container rounded-md flex justify-center items-center mx-auto content-div'>

                   {/* hover effects */}
                    <div className='opacity-0 group-hover:opacity-100'>
                        <span className='text-2xl font-bold text-white tracking-wider flex justify-center items-center'>
                            TaskWise
                        </span>
                        <div className='pt-8 text-center'>
                                <button onClick={() => setShowModal(true)} className='text-center rounded-lg px-4 py-3 m-2 text-gray-700 font-bold text-lg bg-white'>Demo</button>
                            <a href='https://gitlab.com/attack-on-python/attack-on-python' target='_blank' rel='noreferrer'>
                                <button className='text-center rounded-lg px-4 py-3 m-2 text-gray-700 font-bold text-lg bg-white'>Code</button>
                            </a>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <Modal isVisible={showAOP} onClose={() => setShowModal(false)}>
        <div className='flex justify-center items-center'>
            <iframe width="560" height="315" src="https://www.youtube.com/embed/EE-xtCF3T94?si=zd7pLyNl5l2N270N" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
        </div>
        </Modal>
    </div>
  )
}

export default Work