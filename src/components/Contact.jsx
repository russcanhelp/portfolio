import React from 'react'

const Contact = () => {
  return (
    <div name='contact' className='w-full h-screen bg bg-[#272727] text-gray-300 flex justify-center items-center p-4 '>
        <form method='POST' className='flex flex-col max-w-[600px] w-full' action="https://getform.io/f/3140f5ca-0134-4a79-8e2a-9964d08bf00e">
            <div className='pb-4'>
                <p className='text-4xl font-bold inline border-b-4 border-yellow-600 text-gray-300'>Contact</p>
                <p className='text-gray-300 py-4'>Submit the form below or shoot me an email - RussCanHelp@gmail.com</p>
            </div>
            <input className='bg-[#ffffff] p-2 text-black' type='text' placeholder='Name' name='name'/>
            <input className='my-4 p-2 bg-[#ffffff] text-black' type='email' placeholder='Email' name='email' />
            <textarea className='bg-[#ffffff] p-2 text-black' name="message" rows="10" placeholder='Message'></textarea>
            <button className='text-white border-2 hover:bg-yellow-600 hover:border-yellow-600 px-4 py-3 my-8 mx-auto flex items-center'>Let's Collaborate</button>
        </form>
    </div>
  )
}

export default Contact