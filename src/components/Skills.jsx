import React, { useState } from 'react'
import JS from '../assets/JS.png'
import Python from '../assets/Python.png'
import HTML from '../assets/HTML.png'
import CSS from '../assets/CSS.png'
import SQL from '../assets/SQL.png'
import react from '../assets/React.png'
import Modal from './Modal'
import Bootstrap from '../assets/Bootstrap.png'
import Tailwind from '../assets/Tailwind.png'
import Nodejs from '../assets/Node JS.png'
import Django from '../assets/Django.png'
import FastAPI from '../assets/FastAPI.png'
import RabbitMQ from '../assets/RabbitMQ.png'
import Github from '../assets/GitHub.png'
import Docker from '../assets/Docker.png'
import Gitlab from '../assets/GitLab.png'
import Insomnia from '../assets/Insomnia.png'
import SQLite from '../assets/SQLite.png'
import VScode from '../assets/VScode.png'
import Postgres from '../assets/Postgres.png'


const Skills = () => {
    const [showModal, setShowModal] = useState(false)

  return (
    <div name='skills' className= 'w-full h-screen bg-[#272727] text-gray-300 p-8 z-0'>
        {/* container */}
        <div className='max-w-[1000px] mx-auto flex flex-col justify-center w-full h-full'>
            <div>
                <p className='text-4xl font-bold inline border-b-4 border-yellow-600'>Skills</p>
                <p className='py-4'>These are some of the technologies I've worked with</p>
            </div>

            <div className='w-full grid grid-cols-2 sm:grid-cols-3 gap-4 text-center py-4'>
                <div className='shadow-md shadow-[#160b04] hover:scale-110 duration-300 '>
                    <img className='w-20 mx-auto' src={JS} alt='JS icon'/>
                    <p className='my-4'>JavaScript</p>
                </div>
                <div className='shadow-md shadow-[#040c16] hover:scale-110 duration-300 '>
                    <img className='w-20 mx-auto' src={Python} alt='Python icon'/>
                    <p className='my-4'>Python</p>
                </div>
                <div className='shadow-md shadow-[#040c16] hover:scale-110 duration-300 '>
                    <img className='w-20 mx-auto' src={react} alt='react icon'/>
                    <p className='my-4'>React</p>
                </div>
                <div className='shadow-md shadow-[#040c16] hover:scale-110 duration-300 '>
                    <img className='w-20 mx-auto' src={SQL} alt='SQL icon'/>
                    <p className='my-4'>SQL</p>
                </div>
                <div className='shadow-md shadow-[#040c16] hover:scale-110 duration-300 '>
                    <img className='w-20 mx-auto' src={HTML} alt='HTML icon'/>
                    <p className='my-4'>HTML</p>
                </div>
                <div className='shadow-md shadow-[#040c16] hover:scale-110 duration-300 '>
                    <img className='w-20 mx-auto' src={CSS} alt='CSS icon'/>
                    <p className='my-4'>CSS</p>
                </div>
            </div>
            <div className='flex flex-col justify-center items-center'>
                <button onClick={() => setShowModal(true)} className='max-w-[200px] text-white group border-2 px-6 py-3 my-2 flex flex-col items-center justify-center hover:bg-yellow-600 hover:border-yellow-600'>Full Tech Stack</button>
            </div>
        </div>
        <Modal isVisible={showModal} onClose={() => setShowModal(false)}>
        <div className='overflow-auto'>
            <div className='p-4'>
                <h2 className='text-xl font-bold text-center'>Front End</h2>
                <div className='w-auto grid grid-cols-3 sm:grid-cols-4 gap-4 text-center py-2'>
                    <div className='shadow-md shadow-[#040c16] hover:scale-110 duration-300 flex flex-col items-center justify-center'>
                        <img className='w-20 mx-auto' src={JS} alt='JS icon'/>
                        <p className='text-sm pb-1 font-bold'>JavaScript</p>
                    </div>
                    <div className='shadow-md shadow-[#040c16] hover:scale-110 duration-300 flex flex-col items-center justify-center'>
                        <img className='w-20 mx-auto' src={react} alt='react icon'/>
                        <p className='text-sm pb-1'>React</p>
                    </div>
                    <div className='shadow-md shadow-[#040c16] hover:scale-110 duration-300 flex flex-col items-center justify-center'>
                        <img className='w-20 mx-auto' src={HTML} alt='HTML icon'/>
                        <p className='text-sm pb-1'>HTML</p>
                    </div>
                    <div className='shadow-md shadow-[#040c16] hover:scale-110 duration-300 flex flex-col items-center justify-center'>
                        <img className='w-20 mx-auto' src={CSS} alt='CSS icon'/>
                        <p className='text-sm pb-1'>CSS</p>
                    </div>
                    <div className='shadow-md shadow-[#040c16] hover:scale-110 duration-300 flex flex-col items-center justify-center'>
                        <img className='w-20 mx-auto' src={Bootstrap} alt='CSS icon'/>
                        <p className='text-sm pb-1'>Bootstrap</p>
                    </div>
                    <div className='shadow-md shadow-[#040c16] hover:scale-110 duration-300 flex flex-col items-center justify-center'>
                        <img className='w-20 mx-auto' src={Tailwind} alt='CSS icon'/>
                        <p className='text-sm pb-1'>Tailwind</p>
                    </div>
                </div>
            </div>
            <div className='p-4'>
                <h2 className='text-xl font-bold text-center'>Back End</h2>
                <div className='w-auto grid grid-cols-3 sm:grid-cols-4 gap-4 text-center py-2'>
                    <div className='shadow-md shadow-[#040c16] hover:scale-110 duration-300 flex flex-col items-center justify-center'>
                        <img className='w-20 mx-auto' src={Python} alt='Python icon'/>
                        <p className='text-sm pb-1'>Python</p>
                    </div>
                    <div className='shadow-md shadow-[#040c16] hover:scale-110 duration-300 flex flex-col items-center justify-center'>
                        <img className='w-20 mx-auto' src={Django} alt='SQL icon'/>
                        <p className='text-sm pb-1'>Django</p>
                    </div>
                    <div className='shadow-md shadow-[#040c16] hover:scale-110 duration-300 flex flex-col items-center justify-center'>
                        <img className='w-20 mx-auto' src={FastAPI} alt='SQL icon'/>
                        <p className='text-sm pb-1'>Fast API</p>
                    </div>
                    <div className='shadow-md shadow-[#040c16] hover:scale-110 duration-300 flex flex-col items-center justify-center'>
                        <img className='w-20 mx-auto' src={Postgres} alt='SQL icon'/>
                        <p className='text-sm pb-1'>Postgres</p>
                    </div>
                    <div className='shadow-md shadow-[#040c16] hover:scale-110 duration-300 flex flex-col items-center justify-center'>
                        <img className='w-20 mx-auto' src={SQL} alt='SQL icon'/>
                        <p className='text-sm pb-1'>SQL</p>
                    </div>
                    <div className='shadow-md shadow-[#040c16] hover:scale-110 duration-300 flex flex-col items-center justify-center'>
                        <img className='w-20 mx-auto' src={Nodejs} alt='SQL icon'/>
                        <p className='text-sm pb-1'>Node.js</p>
                    </div>
                    <div className='shadow-md shadow-[#040c16] hover:scale-110 duration-300 flex flex-col items-center justify-center'>
                        <img className='w-20 mx-auto' src={RabbitMQ} alt='SQL icon'/>
                        <p className='text-sm pb-1'>RabbitMQ</p>
                    </div>
                </div>
            </div>
            <div className='p-4'>
                <h2 className='text-xl font-bold text-center'>Tools</h2>
                <div className='w-auto grid grid-cols-3 sm:grid-cols-4 gap-4 text-center py-2'>
                    <div className='shadow-md shadow-[#040c16] hover:scale-110 duration-300 flex flex-col items-center justify-center'>
                        <img className='w-20 mx-auto' src={Docker} alt='JS icon'/>
                        <p className='text-sm pb-1'>Docker</p>
                    </div>
                    <div className='shadow-md shadow-[#040c16] hover:scale-110 duration-300 flex flex-col items-center justify-center'>
                        <img className='w-20 mx-auto' src={Gitlab} alt='JS icon'/>
                        <p className='text-sm pb-1'>Gitlab</p>
                    </div>
                    <div className='shadow-md shadow-[#040c16] hover:scale-110 duration-300 flex flex-col items-center justify-center'>
                        <img className='w-20 mx-auto' src={Github} alt='JS icon'/>
                        <p className='text-sm pb-1'>Github</p>
                    </div>
                    <div className='shadow-md shadow-[#040c16] hover:scale-110 duration-300 flex flex-col items-center justify-center'>
                        <img className='w-20 mx-auto' src={Insomnia} alt='JS icon'/>
                        <p className='text-sm pb-1'>Insomnia</p>
                    </div>
                    <div className='shadow-md shadow-[#040c16] hover:scale-110 duration-300 flex flex-col items-center justify-center'>
                        <img className='w-20 mx-auto' src={SQLite} alt='JS icon'/>
                        <p className='text-sm pb-1'>SQLite</p>
                    </div>
                    <div className='shadow-md shadow-[#040c16] hover:scale-110 duration-300 flex flex-col items-center justify-center'>
                        <img className='w-20 mx-auto' src={VScode} alt='JS icon'/>
                        <p className='text-sm pb-1'>VS Code</p>
                    </div>
                </div>
            </div>
        </div>
        </Modal>
    </div>
  )
}

export default Skills